= Simple Revision Control
//  SPDX-FileCopyrightText: Copyright Eric S. Raymond <esr@thyrsus.com>
// SPDX-License-Identifier: BSD-2-Clause

The venerable RCS (Revision Control System) has survived into the era
of distributed version control because it fills a niche: sometimes you
only *want* to track changes in single files at a time - for example,
if you have a directory full of documents with separate histories.

SRC (Simple Revision Control) is RCS, reloaded.  It remains
determinedly file-oriented and doesn't even track the committer of a
change (because that's always you), but incorporates the design and
user-interface lessons of modern version-control systems.  It features
sequential revision numbers, lockless operation, embedded command
help, and a command set that will seem familiar to users of
Subversion, Mercurial, and Git.

See INSTALL.adoc for the (very simple) installation instructions.

Run 'make check' for the regression test.
